﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DmnModelAnalyzer.Research.DomainModels
{
    public class Research
    {
        public int Id { get; set; }
        public int CalibrationLevel { get; set; }
        public string CoordinatesFromCsv { get; set; }
    }
}
